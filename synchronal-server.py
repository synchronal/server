#! /usr/bin/env python

# Copyright 2011 Sam Lade
#
# This file is part of Synchronal Server.
#
# Synchronal Server is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal Server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Synchronal Server.  If not, see <http://www.gnu.org/licenses/>.

# Hardcoded config - you may need to edit this for your server's settings
# CGI error log directory
logdir = "/home/tmp"
# Directory for private libraries
libdir = "/home/protected/synchronal/"

__version__ = "0.2.2"

# API response format:
# Synchronal replies are in JSON with the following format:
# {
#   "synchronal":{
#                   "code":int,
#                   data:val
#                }
# }
# The data item is only returned for certain API methods (see definitions for
# the key name and value in these cases).

# convenience functions for API handlers
def headers():
    """Print basic JSON headers."""
    print "Content-Type: application/json"
    print

def error(errid, errstr):
    """Return an error message.
    
    Provides a non-zero error ID so clients can easily determine what went
    wrong and a string description of the error.
    
    Errors are mostly specified on a per-method basis.
    
    Global errors:
    1 invalid request
    2 server error"""
    headers()
    resp = {"synchronal":{"code":errid, "err":errstr}}
    print json.dumps(resp)

def success(key = "", value = ""):
    """Return a successful operation, optionally with included data"""
    headers()
    if not key:
        resp = {"synchronal":{"code":0}}
    else:
        resp = {"synchronal":{"code":0, key:value}}
    print json.dumps(resp)

def sid_expired(sid):
    """Check if the sid with given issue time has expired"""
    import time
    if (sid + 3600) < time.time():
        return True
    return False

def hash_check(rhash, *args):
    """Validate a supplied SHA1 hash against the expected value"""
    val = "".join(str(i) for i in args)
    import hashlib
    h = hashlib.sha1()
    h.update(val)
    if rhash == h.hexdigest():
        return True
    return False

# API method handlers
def register(req):
    """Register a new user account.
        
    Parameters:
    user = (str <= 40 printable chars)
    pass = (str <= 40 printable chars)
    
    Return:
    (none)
    
    Errors:
    11 invalid username or password
    12 username taken"""

    user = req.getfirst("user")
    passwd = req.getfirst("pass")
    if (not user) | (not passwd):
        error(1, "You must give a username and password.")
        return
    if (len(user) > 40) | (len(passwd) > 40):
        error(11, "Username and password must be 40 chars or less each.")
        return

    db, conn, cursor, ph = initdb()
    try:
        cursor.execute("""insert into users
            (user, pass)
            values ({0}, {0})""".format(ph), (user, passwd))
    except db.IntegrityError:
        error(12, "Username taken.")
        exitdb(conn, cursor)
        return
    conn.commit()
    exitdb(conn, cursor)
    success()

def get_sid(req):
    """Request a new session ID.
    
    Parameters:
    user = (str, registered username)
    
    Return:
    sid
    
    Errors:
    21 unknown username"""
    user = req.getfirst("user")
    if not user:
        error(1, "You must give a username.")
        return

    db, conn, cursor, ph = initdb()
    cursor.execute("""select sid, sid_issue from users where user={0}""".format(ph), (user,))
    res = cursor.fetchone()
    if not res:
        error(21, "Unknown username.")
        exitdb(conn, cursor)
        return

    if not sid_expired(res[1]):
        exitdb(conn, cursor)
        success("sid", res[0])
        return

    import uuid, time
    sid = uuid.uuid4().hex
    issued = int(time.time())

    cursor.execute("""update users set
        sid={0},
        sid_issue={0}
        where user={0}""".format(ph), (sid, issued, user))
    conn.commit()
    exitdb(conn, cursor)
    success("sid", sid)

def expire_sid(req):
    """Tell the server that the session ID is no longer required.
    
    Parameters:
    user = (str, registered username)
    hash = (sha1(session_ID + password + "expire_sid"))

    Return:
    (none)
    
    Errors:
    21 unknown username
    32 invalid hash"""

    user = req.getfirst("user")
    rhash = req.getfirst("hash")
    if (not user) | (not rhash):
        error(1, "You must supply a username and request hash.")
        return

    db, conn, cursor, ph = initdb()
    cursor.execute("""select pass, sid, sid_issue from users
        where user={0}""".format(ph), (user,))
    res = cursor.fetchone()
    if not res:
        error(21, "Unknown username.")
        exitdb(conn, cursor)
        return

    if sid_expired(res[2]):
        # sid already expired, just return success
        success()
        exitdb(conn, cursor)
        return

    if not hash_check(rhash, res[0], res[1], "expire_sid"):
        error(32, "Invalid SHA1 hash. Request new SID, check password, etc.")
        exitdb(conn, cursor)
        return

    cursor.execute("""update users set
        sid='',
        sid_issue=0
        where user={0}""".format(ph), (user,))
    conn.commit()
    exitdb(conn, cursor)
    success()

def get_read(req):
    """Request last read tweet for a given stream.
    
    Parameters:
    user = (str, registered username)
    hash = (sha1(session_ID + password + "get_read" + stream))
    stream = (str <= 64 chars)
    
    stream's definition is up to the client
    
    Return:
    tid = (str)
    
    Errors:
    21 unknown username
    31 SID expired
    32 invalid hash
    33 unknown stream"""

    user = req.getfirst("user")
    rhash = req.getfirst("hash")
    stream = req.getfirst("stream")
    if (not user) | (not rhash) | (not stream):
        error(1, "You must supply a username, stream, and request hash.")
        return

    db, conn, cursor, ph = initdb()
    cursor.execute("""select uid, pass, sid, sid_issue from users
        where user={0}""".format(ph), (user,))
    res = cursor.fetchone()
    if not res:
        error(21, "Unknown username.")
        exitdb(conn, cursor)
        return

    if sid_expired(res[3]):
        error(31, "SID expired. Request a new SID.")
        exitdb(conn, cursor)
        return

    if not hash_check(rhash, res[2], res[1], "get_read", stream):
        error(32, "Invalid SHA1 hash. Request new SID, check password, etc.")
        exitdb(conn, cursor)
        return

    cursor.execute("""select tid from streams
        where uid={0} and stream={0}""".format(ph), (res[0], stream))
    tid = cursor.fetchone()[0]
    if not tid:
        error(33, "Unknown stream. Please set a stream value before retrieving it.")
        exitdb(conn, cursor)
        return

    exitdb(conn, cursor)
    success("tid", tid)

def set_read(req):
    """Set last read tweet for a given stream.
    
    Parameters:
    user = (str, registered username)
    hash = (sha1(session_ID + password + "set_read" + stream + tid))
    stream = (str < 64 chars)
    tid = (str, tweet ID)
    
    stream's definition is up to the client
    
    Return:
    (none)
    
    Errors:
    21 unknown username
    31 SID expired
    32 invalid hash
    41 invalid TID
    42 invalid stream"""

    user = req.getfirst("user")
    rhash = req.getfirst("hash")
    stream = req.getfirst("stream")
    tid = req.getfirst("tid")
    if (not user) | (not rhash) | (not stream) | (not tid):
        error(1, "You must supply a username, stream, Tweet ID, and request hash.")
        return

    try:
        int(tid)
    except ValueError:
        error(41, "The Tweet ID must be an integer in string format.")
        return

    if len(stream) > 64:
        error(42, "Invalid stream. Must be less than 64 characters.")
        return

    db, conn, cursor, ph = initdb()
    cursor.execute("""select uid, pass, sid, sid_issue from users
        where user={0}""".format(ph), (user,))
    res = cursor.fetchone()
    if not res:
        error(21, "Unknown username.")
        exitdb(conn, cursor)
        return

    if sid_expired(res[3]):
        error(31, "SID expired. Request a new SID.")
        exitdb(conn, cursor)
        return

    if not hash_check(rhash, res[2], res[1], "set_read", stream, tid):
        error(32, "Invalid SHA1 hash. Request new SID, check password, etc.")
        exitdb(conn, cursor)
        return

    cursor.execute("""insert or replace into streams
        values ({0}, {0}, {0})""".format(ph), (res[0], stream, tid))
    conn.commit()
    exitdb(conn, cursor)
    success()

def source(req):
    """Returns the server source code as required by the AGPL"""
    print "Content-Type: text/plain"
    print
    for i in ["synchronal-server.py", libdir+"initdb.py", libdir+"dbconf.py"]:
        with open(i) as f:
            print i + ":\n"
            print f.read() + "\n"

methods = {"register":register, "get_sid":get_sid, "expire_sid":expire_sid, "get_read":get_read, "set_read":set_read, "source":source}

import cgi, sys, os.path, json
try:
    # read config, get database helper functions
    sys.path[0] = os.path.expanduser(libdir)
    from dbconf import *
    # get CGI vals
    req = cgi.FieldStorage()

    # call relevant method
    method = req.getfirst("method")
    if method in methods:
        methods[method](req)
    else:
        error(1, "Unknown or unspecified method")

except Exception as err:
    # misc exception handling modified from cgitb with custom messages to browser
    import tempfile, cgitb, os, stat
    try:
        (fd, path) = tempfile.mkstemp(suffix=".txt", dir=logdir)
        doc = cgitb.text(sys.exc_info(), 5)
        file = os.fdopen(fd, 'w')
        file.write(doc)
        error(2, "Server error: {0} {1}. Details saved in {2}.".format(
            str(type(err)).replace("<","&lt;").replace(">","&gt;"), str(err), path))
    except:
        error(2, "Server error: {0} {1}. Details saving failed.".format(
            str(type(err)).replace("<","&lt;").replace(">","&gt;"), str(err)))
    else:
        # make logs readable by all, since they will be owned by the cgi user
        try:
            os.fchmod(fd, os.fstat(fd)[0] | stat.S_IROTH | stat.S_IRGRP)
            file.close()
        except: pass
