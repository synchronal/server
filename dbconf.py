#! /usr/bin/env python

# Copyright 2011 Sam Lade
#
# This file is part of Synchronal Server.
#
# Synchronal Server is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal Server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Synchronal Server.  If not, see <http://www.gnu.org/licenses/>.

# Hardcoded config - you may need to edit this for your server's settings
# CGI error log directory
# Non-standard config file location (default is ~/.synchronal/synchronal.conf)
conffile = "/home/protected/synchronal/synchronal.conf"

# database access
def initdb():
    """Open a database connection.
    
    Returns the connection, a cursor, and the placeholder to be used in SQL
    strings.
    Uses the config file to determine whether to open a MySQL or SQLite
    database and the parameters required."""
    if conf.get("db", "type").lower() == "mysql":
        import MySQLdb as db
        conn = db.connect(host = conf.get("db", "host"),
                            user = conf.get("db", "user"),
                            passwd = conf.get("db", "pass"),
                            db = conf.get("db", "db"))
        ph = "%s"
    else:
        import sqlite3 as db
        conn = db.connect(os.path.expanduser(conf.get("db", "file")))
        ph = "?"
    return db, conn, conn.cursor(), ph

def exitdb(conn, cursor):
    """Closes the given database connection and cursor."""
    cursor.close()
    conn.close()

import ConfigParser, os.path

# read config
conf = ConfigParser.SafeConfigParser()
conf.read([os.path.expanduser("~/.synchronal/synchronal.conf"), conffile])
