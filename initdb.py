#! /usr/bin/env python

# Copyright 2011 Sam Lade
#
# This file is part of Synchronal Server.
#
# Synchronal Server is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal Server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Synchronal Server.  If not, see <http://www.gnu.org/licenses/>.
from dbconf import *
db, conn, cursor, ph = initdb()

cursor.execute("""create table users (
    uid integer primary key,
    user text(40) unique not null,
    pass text(40) not null,
    sid text(32),
    sid_issue integer)""")

cursor.execute("""create table streams (
    uid integer,
    stream text(64) not null,
    tid text(32),
    primary key(uid, stream),
    foreign key(uid) references users(uid))""")
conn.commit()

exitdb(conn, cursor)

if conf.get("db", "type").lower() != "mysql":
    dest = conf.get("db", "file")
    import os, stat
    perm = os.stat(os.path.expanduser(dest))[0]
    os.chmod(os.path.expanduser(dest), perm | stat.S_IWOTH)
